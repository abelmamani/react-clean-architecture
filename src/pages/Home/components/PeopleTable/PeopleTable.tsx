import { People } from '@/models';
import { addFavorites } from '@/redux/states/favoritesSlice';
import { AppStore } from '@/redux/store';
import { Favorite, FavoriteBorder } from '@mui/icons-material';
import { Checkbox } from '@mui/material';
import { DataGrid, GridRenderCellParams } from '@mui/x-data-grid';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

export type PeopleTableProps = {
}

const PeopleTable: React.FC<PeopleTableProps>  = ({}) => {
	//const [selectedPeople, setSelectedPeople] =  useState<People[]>([]);
	const pageSize: number = 5;
	const dispatch = useDispatch();	
	const statePeoples: People[] = useSelector((store: AppStore) => store.peoples);
	const stateFavorites = useSelector((store: AppStore) => store.favorites);
	
	const findPeople = (people: People): boolean => !!stateFavorites.find( p => p.id === people.id);
	const filterPeople = (people: People): People[] => stateFavorites.filter( p => p.id != people.id);
	
	const handleChange = (people: People): void => {
		const filteredPeoples = findPeople(people) ? filterPeople(people) : [...stateFavorites, people];
		dispatch(addFavorites(filteredPeoples));
		//setSelectedPeople(filteredPeoples);
	};
	const columns = [
		{
			field: 'actions',
			type: 'actions',
			sortable: false,
			headerName: '',	
			width: 50,
			renderCell: (params: GridRenderCellParams) => (<>{<Checkbox size='small' icon={<FavoriteBorder />} checkedIcon={<Favorite />} checked = {findPeople(params.row)} onChange={()=>handleChange(params.row)} />}</>)
			
		},
		{
			field: 'name',
			headerName: 'Name',
			flex: 1,
			minWidth: 150,
		},
		{
			field: 'category',
			headerName: 'Categories',
			flex: 1,
			minWidth: 150,
		},
		{
			field: 'company',
			headerName: 'Companies',
			flex: 1,
			minWidth: 150,
		},
		{
			field: 'levelOfHappiness',
			headerName: 'Level of happiness',
			flex: 1,
			renderCell: (params: GridRenderCellParams) => <>{params.value}</>
		  }
	];

	//useEffect(() => {
	//	setSelectedPeople(stateFavorites);
	 // }, [stateFavorites]);
	return (
		<DataGrid 
		rows={statePeoples}
		columns={columns}
		disableColumnSelector 
		disableRowSelectionOnClick
		autoHeight
		initialState={{
			pagination: {
			  paginationModel: {
				pageSize: pageSize,
			  },
			},
		  }}
		pageSizeOptions={[pageSize]}
		getRowId={(row: any)=> row.id}
		/>
	);
};

export default PeopleTable;
