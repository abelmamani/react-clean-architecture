import { peoples } from '@/data';
import { addPeoples } from '@/redux/states/peoplesSlice';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { PeopleTable } from './components';
export type HomeProps = {
}

const Home: React.FC<HomeProps>  = ({}) => {
	const dispatch = useDispatch();
	useEffect(()=>{
		dispatch(addPeoples(peoples));
	}, []);
	
	return (<PeopleTable/>);
};

export default Home;
