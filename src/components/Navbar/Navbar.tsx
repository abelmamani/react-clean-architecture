import FavoriteIcon from '@mui/icons-material/Favorite';
import { IconButton } from '@mui/material';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import React from 'react';
import { CustomDialog } from '../CustomDialog';
import { dialogOpenSubject$ } from '../CustomDialog/CustomDialog';
import { FavoriteTable } from './FavoriteTable';
export type NavbarProps = {
}

const Navbar: React.FC<NavbarProps>  = ({}) => {
  const handleClick = () => {
    dialogOpenSubject$.setSubject = true;
  };

	return (
		<>
    <CustomDialog>
      <FavoriteTable/>
    </CustomDialog>
      <AppBar position="fixed">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            CainMac
          </Typography>
          <IconButton color="secondary" aria-label="favorites" component="label" onClick={handleClick}>
            <FavoriteIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      </>
	);
};

export default Navbar;
