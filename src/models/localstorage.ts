export enum LocalStorageTypes {
    PEOPLES = 'peoples',
    FAVORITES = 'favourites'
}