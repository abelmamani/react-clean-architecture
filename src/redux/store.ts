import { People } from "@/models";
import { configureStore } from "@reduxjs/toolkit";
import peopleSlice from "./states/peoplesSlice";
import favoritesSlice from "./states/favoritesSlice";


export interface AppStore{
    peoples: People[];
    favorites: People[];
}
export default configureStore<AppStore>({
    reducer: {
        peoples: peopleSlice,
        favorites: favoritesSlice
    }
})