import { LocalStorageTypes, People} from '@/models';
import { getLocalStorage, setLocalStorage } from '@/utilities';
import { createSlice } from '@reduxjs/toolkit';
const initialState: People[] = [];

export const peopleSlice = createSlice({
    name: 'peoples',
    initialState: getLocalStorage(LocalStorageTypes.PEOPLES) ? JSON.parse(getLocalStorage(LocalStorageTypes.PEOPLES) as string): initialState,
    reducers: {
        addPeoples: (state, action) => {
            setLocalStorage(LocalStorageTypes.PEOPLES, action.payload);
            return action.payload;
        }
    }
});

export const {addPeoples} = peopleSlice.actions;
export default peopleSlice.reducer;