
import { LocalStorageTypes, People } from '@/models';
import { getLocalStorage, setLocalStorage } from '@/utilities';
import { createSlice, current } from '@reduxjs/toolkit';
const initialState: People[] = [];

export const favoritesSlice = createSlice({
    name: 'favorites',
    initialState: getLocalStorage(LocalStorageTypes.FAVORITES) ? JSON.parse(getLocalStorage(LocalStorageTypes.FAVORITES) as string): initialState,
    reducers: {
        addFavorites: (state, action) => {
            setLocalStorage(LocalStorageTypes.FAVORITES, action.payload);
            return action.payload;
        },
        removeFavorites: (state, action) => {
            const filteredState = current(state).filter((p: People) => p.id !== action.payload.id);
            setLocalStorage(LocalStorageTypes.FAVORITES, filteredState);
            return filteredState;
        }
    }
});

export const {addFavorites, removeFavorites} = favoritesSlice.actions;
export default favoritesSlice.reducer;